from unittest.mock import Mock

from readability.models.readability_bert.bert import (
    max_pooling,
    mean_pooling,
    min_pooling,
    predict_sentences_scores,
)


def test_generate_predict_sentences_scores():
    input_sentences = ["sentence 1", "sentence 2", "sentence 3", "sentence 4"]
    pipeline_output_example = [
        [{"score": 0.2, "label": "LABEL_1"}, {"score": 0.8, "label": "LABEL_0"}],
        [{"score": 0.1, "label": "LABEL_1"}, {"score": 0.9, "label": "LABEL_0"}],
        [{"score": 0.2, "label": "LABEL_0"}, {"score": 0.8, "label": "LABEL_1"}],
        [{"score": 0.1, "label": "LABEL_0"}, {"score": 0.9, "label": "LABEL_1"}],
    ]
    expected_output = [0.2, 0.1, 0.8, 0.9]
    model_mock = Mock()
    model_mock.return_value = pipeline_output_example
    assert predict_sentences_scores(model_mock, input_sentences) == expected_output


def test_mean_pooling():
    input_fallback = []
    input_regular = [0.2, 0.4, 0.6, 0.8]
    assert mean_pooling(input_regular) == 0.5
    assert mean_pooling(input_fallback) == 0.0


def test_min_pooling():
    input_fallback = []
    input_regular = [0.2, 0.4, 0.6, 0.8]
    assert min_pooling(input_regular) == 0.2
    assert min_pooling(input_fallback) == 0.0


def test_max_pooling():
    input_fallback = []
    input_regular = [0.2, 0.4, 0.6, 0.8]
    assert max_pooling(input_regular) == 0.8
    assert max_pooling(input_fallback) == 0.0
