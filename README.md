# readability-liftwing

## About
`readability-liftwing` is a repository that implements the logic of readability score prediction for wikipedia revision.

## Package installation
To install, run:
```bash
pip install https://gitlab.wikimedia.org/trokhymovych/readability-liftwing.git@v0.1.3
```

## Usage
#### Activating virtual environment:
```bash
make activate_virtual_env
```
#### Usage example:
```python
import asyncio
import pathlib

import aiohttp
import mwapi

from knowledge_integrity.revision import get_current_revision
from readability.models.readability_bert import classify, load_model

model_path = 'multilingual_readability_model_v1.pkl'
model = load_model(pathlib.Path(model_path))

async def main() -> None:
    async with aiohttp.ClientSession() as s:
        session = mwapi.AsyncSession(
            "https://en.wikipedia.org",
            session=s,
        )
        revision = await get_current_revision(session, 3423342, "en")
        classification_result = classify(model, revision)
        print(classification_result)

asyncio.run(main())
```

### Creating model binary script with pretrained ranking model:
```bash

python readability/binary_setup.py  -rm models/Transformer_based_ranker.mod -sm models/fk_model.pkl -rb Peltarion/xlm-roberta-longformer-base-4096 -name binaries/multilingual_readability_model_v4.bin -tmp models
```

## Development
### Prerequisites
* Install the latest version of [Poetry](https://python-poetry.org/docs/#installation)

### Setup
After installing the prerequisites, run:
```bash
git clone https://gitlab.wikimedia.org/trokhymovych/readability-liftwing.git
cd readability-liftwing
make install
```
### Linter: 
Automated formatting
```bash
make format
```
Checking formatting: 
```bash
make lint
```
### Testing
To run all tests:
```bash
make test
```

To run all tests with html coverage report:
```bash
make test_cov
```
