lint: ## Lint whole python project
	@echo "Run linter readability and tests folders"
	flake8 readability/ tests/
	isort --check-only --diff --stdout .
	mypy readability/
	@echo "Done"

format: ## Format python code
	isort .

install: ## Create virtual environment and setup requirements
	@echo "Setup poetry virtual environment"
	poetry install
	@echo "Done"

activate_virtual_env: ## Activate virtual environment
	@echo "Activating poetry virtual environment"
	poetry shell
	@echo "Done"

test: ## Run test suit
	@echo "Running tests..."
	pytest tests --cov=readability
	@echo "Done test run"

test_cov: ## Run test suit
	@echo "Running tests..."
	pytest tests --cov=readability --cov-report=html
	@echo "Done test run"

ci_lint: ## Lint whole python package in CI runner
	poetry run flake8 readability/ tests/
	poetry run isort --check-only --diff --stdout .
	poetry run mypy readability/

ci_test: ## Lint whole python package in CI runner
	poetry run pytest tests/ --cov=readability
