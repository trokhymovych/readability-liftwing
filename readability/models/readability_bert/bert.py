from typing import Any

import numpy as np  # type: ignore
import transformers  # type: ignore


def predict_sentences_scores(
    model: transformers.Pipeline,
    sentences: list[str],
    truncation: bool = True,
    max_length: int = 512,
    batch_size: int = 128,
) -> list[float]:
    """
    Method that gets the scores for list of sentences using model (transformer pipeline)
    """
    tokenizer_kwargs = {'truncation': truncation, 'max_length': max_length}
    raw_predictions = model(sentences, top_k=2, **tokenizer_kwargs, batch_size=batch_size)
    sentences_scores = [
        [score['score'] for score in raw_score if score['label'] == "LABEL_1"][0]
        for raw_score in raw_predictions
    ]
    return sentences_scores


def mean_pooling(sentences_scores: list[float], **kwargs: Any) -> float:
    """
    Method that aggregate the scores for the sentences using mean
    """
    if len(sentences_scores) > 0:
        return float(np.mean(sentences_scores))
    else:
        return 0


def max_pooling(sentences_scores: list[float], **kwargs: Any) -> float:
    """
    Method that aggregate the scores for the sentences using max
    """
    if len(sentences_scores) > 0:
        return float(np.max(sentences_scores))
    else:
        return 0


def min_pooling(sentences_scores: list[float], **kwargs: Any) -> float:
    """
    Method that aggregate the scores for the sentences using min
    """
    if len(sentences_scores) > 0:
        return float(np.min(sentences_scores))
    else:
        return 0
