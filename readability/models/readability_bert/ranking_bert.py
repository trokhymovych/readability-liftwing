from typing import Any

import torch
import torch.nn as nn

from transformers import AutoModel, AutoTokenizer  # type: ignore


class ReadabilityModel(nn.Module):
    def __init__(self, model_name: str) -> None:
        super(ReadabilityModel, self).__init__()
        self.model = AutoModel.from_pretrained(model_name)
        self.drop = nn.Dropout(p=0.2)
        self.fc = nn.Linear(768, 1)

    def forward(self, ids: Any, mask: Any) -> Any:
        out = self.model(input_ids=ids, attention_mask=mask,
                         output_hidden_states=False)
        out = self.drop(out[1])
        outputs = self.fc(out)
        return outputs


def predict_text_scores(
    model: ReadabilityModel,
    tokenizer: AutoTokenizer,
    text: str,
    truncation: bool = True,
    max_length: int = 512
) -> float:
    """
    Method that gets the scores for list of sentences using model (transformer pipeline)
    """
    with torch.no_grad():
        encoded_input = tokenizer(
            text,
            truncation=truncation,
            add_special_tokens=True,
            max_length=max_length,
            padding='max_length',
            return_tensors='pt'
        )
        predicted_score = model(encoded_input["input_ids"], encoded_input["attention_mask"]).flatten().numpy()[0]
    return predicted_score
