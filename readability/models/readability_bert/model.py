import logging
import pathlib
import sys

from dataclasses import dataclass
from typing import Any, List, Sequence

import joblib  # type: ignore

from knowledge_integrity.featureset import ContentFeatures, FeatureSource, get_features
from knowledge_integrity.revision import CurrentRevision  # type: ignore
from sklearn.linear_model import LinearRegression  # type: ignore
from transformers import AutoTokenizer  # type: ignore

from readability.models.readability_bert.ranking_bert import (
    ReadabilityModel,
    predict_text_scores,
)
from readability.utils import wikitext2text


MODEL_FEATURES = ["revision_text"]
MODEL_VERSION: int = 4
DECISION_THRESHOLD = 0
MAX_TOKENS = 1500


@dataclass
class MultilingualTextReadabilityModel:
    model_version: int
    tokenizer: AutoTokenizer
    scorer: ReadabilityModel
    fk_scorer: LinearRegression
    supported_wikis: List[str]


@dataclass
class ReadabilityResult:
    score: float
    fk_score_proxy: float


def _transformed_readability_features(features: dict[str, Any]) -> dict[str, Any]:
    text = wikitext2text(features["wikitext"])
    return dict(
        revision_text=text,
    )


def extract_features(
    revision: CurrentRevision, features: Sequence[str]
) -> dict[str, Any]:
    # groups of features required for classification model
    content_features = ContentFeatures(revision)
    feature_sources = (FeatureSource(source=content_features),)
    return get_features(feature_sources, features, _transformed_readability_features)


def load_model(model_path: pathlib.Path) -> MultilingualTextReadabilityModel:
    # ensure that joblib can find the model in global symbols when unpickling
    sys.modules["__main__"].MultilingualTextReadabilityModel = MultilingualTextReadabilityModel  # type: ignore  # noqa: E501
    sys.modules["__main__"].ReadabilityModel = ReadabilityModel  # type: ignore  # noqa: E501

    with open(model_path, "rb") as f:
        model: MultilingualTextReadabilityModel = joblib.load(f)

    # Setting model to eval mode
    model.scorer.eval()

    if model.model_version != MODEL_VERSION:
        logging.warn(
            "Serialized model version %d does not match current model version %d",
            model.model_version,
            MODEL_VERSION,
        )
    return model


def classify(model: MultilingualTextReadabilityModel,
             revision: CurrentRevision,
             num_threads: int = -1) -> ReadabilityResult:

    readability_features = extract_features(revision, MODEL_FEATURES)

    # preparing features for fk_scorer
    text_score = predict_text_scores(
        model=model.scorer,
        tokenizer=model.tokenizer,
        text=readability_features["revision_text"],
        max_length=MAX_TOKENS
    )
    text_fk_score = model.fk_scorer.predict([[text_score]])[0]

    return ReadabilityResult(
        # convert numpy values to make them serializable
        score=float(text_score),
        fk_score_proxy=float(text_fk_score),
    )
