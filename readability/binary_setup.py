import argparse
import pickle

import joblib  # type: ignore
import torch

from readability_experiments.models.transformer_ranker import (  # type: ignore
    TRRankerTextBased,
)

from readability.models.readability_bert.model import MultilingualTextReadabilityModel
from readability.models.readability_bert.ranking_bert import ReadabilityModel


MODEL_VERSION = 4
SUPPORTED_WIKIS = [
    'af', 'sq', 'am', 'ar', 'hy',
    'as', 'az', 'eu', 'be', 'bn',
    'bs', 'br', 'bg', 'my',
    'ca', 'zh-yue', 'zh', 'zh-classical', 'hr',
    'cs', 'da', 'nl', 'en', 'eo',
    'et', 'tl', 'fi', 'fr', 'gl',
    'ka', 'de', 'el', 'gu', 'ha',
    'he', 'hi', 'hu', 'is',
    'id', 'ga', 'it', 'ja', 'jv',
    'kn', 'kk', 'km', 'ko', 'ku',
    'ky', 'lo', 'la', 'lv', 'lt',
    'mk', 'mg', 'ms', 'ml', 'mr',
    'mn', 'ne', 'no', 'or', 'om',
    'ps', 'fa', 'pl', 'pt', 'pa',
    'ro', 'ru', 'sa', 'gd', 'sr',
    'sd', 'si', 'sk', 'sl', 'so',
    'es', 'su', 'sw', 'sv', 'ta',
    'te', 'th', 'tr',
    'uk', 'ur', 'ug', 'uz',
    'vi', 'cy', 'fy', 'xh', 'yi', 'simple'
]


# Parsing arguments
parser = argparse.ArgumentParser(description='Script for creating a model binary')
parser.add_argument('-rm', '--ranker', help='Ranking model path', required=True)
parser.add_argument('-rb', '--ranker_base', help='Base ranking model', required=True)
parser.add_argument('-sm', '--scorer', help='Transformation model to FK-score path', required=True)
parser.add_argument('-name', '--name', help='binary name', required=True)
parser.add_argument('-tmp', '--tmp_dir', help='temporary files directory', required=True)

# Getting arguments
args = vars(parser.parse_args())
model_path = args["ranker"]
base_model = args["ranker_base"]
fk_scorer_path = args["scorer"]
binary_model_path = args["name"]
tmp_dir = args["tmp_dir"]

print(args)


# Get the output of readability_experiments module (used for training the model)
model = TRRankerTextBased.load(model_path)
# move model.model to cpu
model.model = model.model.to('cpu')
# Save model.model (saving only weights)
torch.save(model.model.state_dict(), f'{tmp_dir}/transformer_based_ranker_model_v1.pth')

# Build model class for inference
rmodel = ReadabilityModel(base_model)
state_dict = torch.load(f'{tmp_dir}/transformer_based_ranker_model_v1.pth')
rmodel.load_state_dict(state_dict)

# load regression model
reg_model = pickle.load(open(fk_scorer_path, 'rb'))


# Loading the model:
model_instance = MultilingualTextReadabilityModel(
    model_version=MODEL_VERSION,
    tokenizer=model.tokenizer,
    scorer=rmodel,
    fk_scorer=reg_model,
    supported_wikis=SUPPORTED_WIKIS
)
joblib.dump(model_instance, binary_model_path, compress=9)
